# RESTAURANT MANAGEMENT

## Build
write this on your command line (if you already had Maven environment)
```bash
mvn install
mvnw Spring-boot:run 
```

## Entity
```java
Menu(Id, Name, Price, Description)
MenuItems(Id, MenuID, BillID, Quantỉy)
Bill(ID, OrderTime, Total)
```
## Repository
JpaRepository
## Service
### Menu
Create/ Update/ Delete/ FindMenuByID/ GetAllMenu
### MenuItems
Create/ update/ delete
### Bill
Create/ Update/ Delete/ FindBillByID/ GetAllBill
