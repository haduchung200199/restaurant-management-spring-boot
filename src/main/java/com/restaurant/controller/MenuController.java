package com.restaurant.controller;

import com.restaurant.dto.MenuDTO;
import com.restaurant.exception.type.NotFoundException;
import com.restaurant.exception.type.RestaurantException;
import com.restaurant.services.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;

import static com.restaurant.controller.MenuController.MENU_URL;

/**
 * Menu controller
 */
@RestController
@RequestMapping(MENU_URL)
public class MenuController {
    //Constant URL for Menu
    public static final String MENU_URL = "/api/restaurant/menu";

    @Autowired
    private MenuService menuService;

    /**
     * get all menus
     */
    @GetMapping
    public ResponseEntity<Page<MenuDTO>>  getAllMenus(
            @RequestParam Optional<Integer> page,
            @RequestParam Optional<String> sort,
            @RequestParam Optional<Integer> size) {
        return new ResponseEntity(menuService.getAllMenu(page, sort,size), HttpStatus.OK);
    }

    /**
     * get menu by id
     */
    @GetMapping("/id/{id}")
    public ResponseEntity<MenuDTO> getMenuById(@PathVariable int id) throws NotFoundException {
        return new ResponseEntity(menuService.findMenuById(id), HttpStatus.OK);
    }

    /**
     * DeleteMenu by id
     */
    @DeleteMapping("/id/{id}")
    public ResponseEntity<Void> deleteMenuById(@PathVariable int id) throws NotFoundException, RestaurantException {
        menuService.deleteMenuById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * update Menu by Id
     */
    @PutMapping("/id/{id}")
    public ResponseEntity<MenuDTO> updateMenuById(@PathVariable int id, @RequestBody MenuDTO menuDTO) throws NotFoundException,RestaurantException{
        return new ResponseEntity(menuService.updateMenu(id,menuDTO),HttpStatus.OK);
    }

    /**
     * create new menu
     */
    @PostMapping
    public ResponseEntity<MenuDTO> createNewMenu(@RequestBody MenuDTO menuDTO) throws RestaurantException {
        return new ResponseEntity(menuService.createNewMenu(menuDTO), HttpStatus.OK);
    }
}
