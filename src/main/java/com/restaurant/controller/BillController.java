package com.restaurant.controller;

import com.restaurant.dto.BillDTO;
import com.restaurant.dto.MenuItemDTO;
import com.restaurant.exception.type.NotFoundException;
import com.restaurant.exception.type.RestaurantException;
import com.restaurant.services.BillService;
import com.restaurant.services.MenuItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;
import static com.restaurant.controller.BillController.BILL_URL;

/**
 * Controller for Bill
 */
@Controller
@RequestMapping(BILL_URL)
public class BillController {
    public static final String BILL_URL = "/api/restaurant/bill";
    @Autowired
    BillService billService;
    @Autowired
    MenuItemService menuItemService;

    /**
     * Create new Bill
     */
    @PostMapping
    public ResponseEntity createBill() {
        return new ResponseEntity(billService.createNewBill(),HttpStatus.OK);
    }

    /**
     * Delete menuItem by Id
     */
    @DeleteMapping("/id/{billId}/menuitem/id/{menuItemId}")
    public ResponseEntity<Void> deleteMenuItemById(@PathVariable int billId, @PathVariable int menuItemId) throws NotFoundException {
        menuItemService.deleteMenuItemById(billId,menuItemId);
        return new ResponseEntity(HttpStatus.OK);

    }

    /**
     * Delete Bill by Id
     */
    @DeleteMapping("/id/{id}")
    public ResponseEntity<Void> deleteBillById(@PathVariable int id) throws NotFoundException, RestaurantException {
        billService.deleteBillById(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * get Bill by id
     */
    @GetMapping("/id/{id}")
    public ResponseEntity<BillDTO> getBillById(@PathVariable int id) throws NotFoundException{
        return new ResponseEntity(billService.findBillById(id),HttpStatus.OK);
    }

    /**
     *Get all Bill
     */
    @GetMapping
    public ResponseEntity<Page<BillDTO>> getAllBill(
            @RequestParam Optional<Integer> page,
            @RequestParam Optional<String> sort,
            @RequestParam Optional<Integer> size){
        return new ResponseEntity(billService.getAllBill(page,sort,size),HttpStatus.OK);
    }
    /**
     * Update MenuItem By Id
     */
    @PutMapping("/id/{billId}/menuitem/id/{menuItemId}")
    public ResponseEntity<BillDTO> updateBill(@PathVariable int billId,
                                                  @PathVariable int menuItemId,
                                                  @RequestBody MenuItemDTO menuItemDTO) throws NotFoundException{
        return new ResponseEntity(menuItemService.updateMenuItemById(billId, menuItemId, menuItemDTO),HttpStatus.OK);
    }

    /**
     * Put Bill by adding new MenuItem to Bill
     * The same menu will be added to quantity.
     */
    @PutMapping("/id/{id}")
    public ResponseEntity<BillDTO> addBill(@PathVariable int id, @RequestBody BillDTO billDTO) throws NotFoundException, RestaurantException{
        return new ResponseEntity(billService.addMenuitemToBill(id, billDTO), HttpStatus.OK);
    }
}
