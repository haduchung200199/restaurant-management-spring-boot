package com.restaurant.entity;

import javax.persistence.*;
import static com.restaurant.entity.Menu.MENU_TABLE;

/**
 * Menu Entity
 */
@Entity
@Table(name = MENU_TABLE) //map table tb_Menu with Menu
public class Menu {
    public static final String MENU_TABLE = "menu";
    public static final String MENU_ID_COLUMN = "id";
    public static final String NAME_COLUMN = "name";
    public static final String TYPE_COLUMN = "type";
    public static final String PRICE_COLUMN = "price";
    public static final String DESCRIPTION_COLUMN = "description";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = MENU_ID_COLUMN)
    private Integer id; //set menuID as primaryKey and auto generate
    @Column(name = NAME_COLUMN)
    private String name;
    @Column(name = TYPE_COLUMN)
    private String type;
    @Column(name = PRICE_COLUMN)
    private Long price;
    @Column(name = DESCRIPTION_COLUMN)
    private String description;

    /**
     * constructor without parameter
     */
    public Menu() {
    }

    /**
     * constructor with all parameter (No parameter of MenuItems)
     */
    public Menu(Integer id, String name, String type, Long price, String description) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.price = price;
        this.description = description;
    }

    /**
     * Getter for Id
     */
    public Integer getId() {
        return id;
    }

    /**
     * setter for id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * getter for name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * setter for name
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * getter for type
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     * setter for type
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * getter for price
     * @return
     */
    public Long getPrice() {
        return price;
    }

    /**
     * setter for price
     * @param price
     */
    public void setPrice(Long price) {
        this.price = price;
    }

    /**
     * getter for description
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * setter for description
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * to string for Menu
     * @return
     */
    @Override
    public String toString() {
        return "Menu{" +
                "menuID=" + id +
                ", name='" + name + '\'' +
                ", menuType='" + type + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                '}';
    }
}
