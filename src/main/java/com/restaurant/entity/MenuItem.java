package com.restaurant.entity;

import com.fasterxml.jackson.annotation.*;
import javax.persistence.*;

import java.util.Objects;

import static com.restaurant.entity.MenuItem.MENU_ITEMS_TABLE;

/**
 * MenuItem entity
 */
@Entity
@Table(name = MENU_ITEMS_TABLE)
public class MenuItem {
    public static final String MENU_ITEMS_TABLE = "menuitems";
    public static final String MENU_ITEM_ID_COLUMN = "id";
    public static final String QUANTITY_COLUMN = "quantity";
    public static final String BILL_ID_COLUMN = "bill_id";
    public static final String MENU_ID_COLUMN = "menu_id";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = MENU_ITEM_ID_COLUMN)
    private Integer id;
    @Column(name = QUANTITY_COLUMN)
    private Integer quantity;

    //join table with
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = BILL_ID_COLUMN)
    private Bill bill;

    //Join table with Menu
    //@JsonBackReference
    @ManyToOne
    @JoinColumn(name = MENU_ID_COLUMN)
    private Menu menu;

    /**
     * No parameter Constructor
     */
    public MenuItem() {
    }

    /**
     *Constructor without  parameter
     */
    public MenuItem(Integer id, Bill bill, Menu menu, Integer quantity) {
        this.id = id;
        this.bill = bill;
        this.menu = menu;
        this.quantity = quantity;
    }

    /**
     * Getter for MenuItem ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setter for MenuItem Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Getter for MenuItem quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * Setter for MenuItem quantity
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     *Getter for MenuItem Bill
     */
    public Bill getBill() {
        return bill;
    }

    /**
     *Setter for MenuItem Bill
     */
    public void setBill(Bill bill) {
        this.bill = bill;
    }
    /**
     *Getter for MenuItem Menu
     */

    public Menu getMenu() {
        return menu;
    }
    /**
     *Setter for MenuItem Menu
     */
    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    /**
     * to string for MenuItem
     */
    @Override
    public String toString() {
        return "MenuItems{" +
                "id=" + id +
                ", quantity=" + quantity +
                ", bill=" + bill +
                ", menu=" + menu +
                '}';
    }

    /**
     * Equal to check if there are the same menu
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MenuItem menuItem = (MenuItem) o;
        return  Objects.equals(menu, menuItem.menu)
                && Objects.equals(bill, menuItem.bill);
    }

    /**
     * Use hash code to avoid the same menu on menuItem
     */
    @Override
    public int hashCode() {
        return Objects.hash(bill, menu);
    }
}
