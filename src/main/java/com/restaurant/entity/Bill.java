package com.restaurant.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Set;

import static com.restaurant.entity.Bill.BILL_TABLE;

/**
 * Bill entity
 */
@Entity
@Table(name = BILL_TABLE)
public class Bill {
    public static final String BILL_TABLE = "bill";
    public static final String ID_COLUMN = "id";
    public static final String ORDER_TIME_COLUMN = "ordertime";
    public static final String TOTAL_COLUMN = "total";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = ID_COLUMN)
    private Integer id;
    @Column(name = ORDER_TIME_COLUMN)
    private String orderTime;
    @Column(name = TOTAL_COLUMN)
    private Long totalBill;
    //join table MenuItem and Bill
    @JsonManagedReference
    @OneToMany(mappedBy = BILL_TABLE,cascade = CascadeType.ALL)
    private Set<MenuItem> menuItem;

    /**
     * Constructor with all parameters (without MenuItems)
     */
    public Bill(Integer id, String orderTime, Set<MenuItem> menuItem, Long totalBill) {
        this.id = id;
        this.orderTime = orderTime;
        this.menuItem = menuItem;
        this.totalBill = totalBill;
    }

    /**
     * getter for menuItem
     */
    public Set<MenuItem> getMenuItem() {
        return menuItem;
    }

    /**
     * setter for menuItem
     */
    public void setMenuItem(Set<MenuItem> menuItems) {
        this.menuItem = menuItems;
    }

    /**
     * Constructor without parameter
     */
    public Bill() {
    }

    /**
     * getter for bill id
     */
    public Integer getId() {
        return id;
    }

    /**
     * setter for bill id
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * getter for bill orderTime
     */
    public String getOrderTime() {
        return orderTime;
    }
    /**
     * setter for bill orderTime
     */
    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }
    /**
     * getter for bill totalBill
     */
    public Long getTotalBill() {
        return totalBill;
    }
    /**
     * setter for bill totalBill
     */
    public void setTotalBill(Long totalBill) {
        this.totalBill = totalBill;
    }

    /**
     * to String for Bill
     */
    @Override
    public String toString() {
        return "Bill{" +
                "id=" + id +
                ", orderTime='" + orderTime + '\'' +
                ", totalBill=" + totalBill +
                '}';
    }
}
