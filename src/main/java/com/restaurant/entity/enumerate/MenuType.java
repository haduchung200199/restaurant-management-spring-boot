package com.restaurant.entity.enumerate;

/**
 * Contains menuType
 */
public enum MenuType {
    Breakfast, Lunch, Dinner, Drink
}
