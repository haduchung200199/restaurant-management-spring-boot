package com.restaurant.dto;

/**
 * Data transfer Object for Menu
 */
public class MenuDTO {
    private Integer id;
    private String name;
    private String type;
    private Long price;
    private String description;
    /**
     * No parameter constructor
     */
    public MenuDTO() {
    }

    /**
     *All parameters constructor
     */
    public MenuDTO(Integer id, String name, String type, Long price, String description) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.price = price;
        this.description = description;
    }

    /**
     * Getter for MenuDTO Id
     */
    public Integer getId() {
        return id;
    }
    /**
     * setter for MenuDTO Id
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * Getter for MenuDTO name
     */
    public String getName() {
        return name;
    }
    /**
     * setter for MenuDTO name
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * Getter for MenuDTO type
     */
    public String getType() {
        return type;
    }
    /**
     * setter for MenuDTO type
     */
    public void setType(String type) {
        this.type = type;
    }
    /**
     * Getter for MenuDTO price
     */
    public Long getPrice() {
        return price;
    }
    /**
     * setter for MenuDTO price
     */
    public void setPrice(Long price) {
        this.price = price;
    }
    /**
     * Getter for MenuDTO description
     */
    public String getDescription() {
        return description;
    }
    /**
     * setter for MenuDTO description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * to string for menuDTO
     */
    @Override
    public String toString() {
        return "MenuDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                '}';
    }
}
