package com.restaurant.dto;

import com.restaurant.entity.Bill;
import com.restaurant.entity.Menu;

/**
 * Data transfer Object for MenuItem
 */
public class MenuItemDTO {
    private Integer id;
    private Bill bill;
    private Menu menu;
    private Integer quantity;

    /**
     * MenuItems constructor with no parameter
     */
    public MenuItemDTO() {
    }

    /**
     *Constructor with all parameters
     */
    public MenuItemDTO(Integer id, Bill bill, Menu menu, Integer quantity) {
        this.id = id;
        this.bill = bill;
        this.menu = menu;
        this.quantity = quantity;
    }

    /**
     * getter for MenuItemDTO id
     */
    public Integer getId() {
        return id;
    }
    /**
     * setter for MenuItemDTO id
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * getter for MenuItemDTO Bill
     */
    public Bill getBill() {
        return bill;
    }
    /**
     * setter for MenuItemDTO Bill
     */
    public void setBill(Bill bill) {
        this.bill = bill;
    }
    /**
     * getter for MenuItemDTO Menu
     */
    public Menu getMenu() {
        return menu;
    }
    /**
     * setter for MenuItemDTO Menu
     */
    public void setMenu(Menu menu) {
        this.menu = menu;
    }
    /**
     * getter for MenuItemDTO quantity
     */
    public Integer getQuantity() {
        return quantity;
    }
    /**
     * setter for MenuItemDTO quantity
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * to string for MenuItemDTO
     */
    @Override
    public String toString() {
        return "MenuItemDTO{" +
                "id=" + id +
                ", bill=" + bill +
                ", menu=" + menu +
                ", quantity=" + quantity +
                '}';
    }
}
