package com.restaurant.dto;

import com.restaurant.entity.MenuItem;

import java.util.Set;

/**
 * Data transfer Object for Bill
 */
public class BillDTO {
    private Integer id;
    private String orderTime;
    private Long totalBill;
    private Set<MenuItem> menuItem;
    /**
     * No parameter constructor
     */
    public BillDTO() {
    }

    /**
     * All parameters constructor
     */
    public BillDTO(Integer id, String orderTime,Set<MenuItem> menuItem, Long totalBill) {
        this.id = id;
        this.orderTime = orderTime;
        this.menuItem = menuItem;
        this.totalBill = totalBill;
    }

    /**
     * getter for menuItem
     */
    public Set<MenuItem> getMenuItem() {
        return menuItem;
    }

    /**
     * Setter for menuItem
     */
    public void setMenuItem(Set<MenuItem> menuItem) {
        this.menuItem = menuItem;
    }

    /**
     *Getter for BillDTO Id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setter for BillDTO Id
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * getter for BillDTO date
     */
    public String getOrderTime() {
        return orderTime;
    }
    /**
     * setter for BillDTO date
     */
    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }
    /**
     * getter for BillDTO totalBill
     */
    public Long getTotalBill() {
        return totalBill;
    }
    /**
     * Setter for BillDTO totalBill
     */
    public void setTotalBill(Long totalBill) {
        this.totalBill = totalBill;
    }

    /**
     *To string for Bill DTO
     */
    @Override
    public String toString() {
        return "BillDTO{" +
                "id=" + id +
                ", date='" + orderTime + '\'' +
                ", totalBill=" + totalBill +
                '}';
    }
}
