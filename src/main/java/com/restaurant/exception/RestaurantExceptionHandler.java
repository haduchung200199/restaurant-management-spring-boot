package com.restaurant.exception;

import com.restaurant.exception.type.NotFoundException;
import com.restaurant.exception.type.RestaurantException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * This class is a global exception handler
 * Handle exceptions from Restaurant exception and NotFoundException
 */
@ControllerAdvice
public class RestaurantExceptionHandler {
    /**
     *Handle NotFoundException. return NotFound status
     */
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<String> handleException(NotFoundException exception){
        return new ResponseEntity(exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    /**
     *Handle RestaurantException return BadRequest status
     */
    @ExceptionHandler(RestaurantException.class)
    public ResponseEntity<String> handleException(Exception exception){
        return new ResponseEntity(exception.getMessage(),HttpStatus.BAD_REQUEST);
    }
}
