package com.restaurant.exception.type;

/**
 * throws when can not find object (menu, bill, menuItem)
 */
public class NotFoundException extends Exception {

    /**
     *Exception that can return message
     */
    public NotFoundException(String message){
        super(message);
    }
}
