package com.restaurant.exception.type;

/**
 * Restaurant exception is an exception about BadRequest while using app
 */
public class RestaurantException extends Exception {

    /**
     * Exception that can return message
     */
    public RestaurantException(String message) {
        super(message);
    }
}
