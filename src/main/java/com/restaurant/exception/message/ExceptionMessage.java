package com.restaurant.exception.message;

/**
 * Contains messages for exceptions
 */
public class ExceptionMessage {
    //RestaurantException

    //Wrong Bill Id
    public final String WRONG_BILL_ID_EXCEPTION_MESSAGE = "Invalid Bill id. Bill id in url must be the same with Bill id in json";
    //Wrong MenuType
    public final String WRONG_MENU_TYPE_EXCEPTION_MESSAGE = "Invalid Menu type, Menu type must only contains [Breakfast, Lunch, Dinner or Drink]";
    //Menu is on used
    public final String MENU_IS_BEING_USED_EXCEPTION_MESSAGE = "Menu is being used";
    //Menu exist
    public final String MENU_EXIST_EXCEPTION_MESSAGE = "Menu is already exist";
    //Bill is on used
    public final String BILL_IS_BEING_USED_EXCEPTION_MESSAGE = "Bill is being used";
    /////////////////////////////////////////////////////////////////////////////////////////////////

    //NotFoundException

    //Can not find Bill
    public final String CAN_NOT_FIND_BILL_EXCEPTION_MESSAGE = "Can not find Bill";
    //Can not find Menu
    public final String CAN_NOT_FIND_MENU_EXCEPTION_MESSAGE = "Can not find Menu";
    //Can not find MenuItem
    public final String CAN_NOT_FIND_MENUITEM_EXCEPTION_MESSAGE = "Can not find MenuItem";
}
