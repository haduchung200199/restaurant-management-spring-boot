package com.restaurant.services;

import com.restaurant.dto.BillDTO;
import com.restaurant.exception.type.NotFoundException;
import com.restaurant.exception.type.RestaurantException;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

/**
 * Service for Bill
 */
public interface BillService {
    BillDTO createNewBill();
    List<BillDTO> getAllBill();
    Page<BillDTO> getAllBill(Optional<Integer> page, Optional<String> sortBy, Optional<Integer> size);
    BillDTO findBillById(int id) throws NotFoundException;
    void deleteBillById(int id) throws NotFoundException, RestaurantException;
    BillDTO addMenuitemToBill(int id, BillDTO billDTO) throws NotFoundException,RestaurantException;
}
