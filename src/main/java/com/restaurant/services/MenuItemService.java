package com.restaurant.services;

import com.restaurant.dto.MenuItemDTO;
import com.restaurant.exception.type.NotFoundException;

/**
 * Service for MenuItems
 */
public interface MenuItemService {
    void deleteMenuItemById(int billId, int menuItemId) throws NotFoundException;
    MenuItemDTO updateMenuItemById(int billId, int menuItemId, MenuItemDTO menuItemDTO) throws NotFoundException;
}
