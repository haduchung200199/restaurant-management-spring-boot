package com.restaurant.services;

import com.restaurant.dto.MenuDTO;
import com.restaurant.exception.type.NotFoundException;
import com.restaurant.exception.type.RestaurantException;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

/**
 * Interface contains services for menu
 */
public interface MenuService {
    MenuDTO createNewMenu(MenuDTO menuDTO) throws RestaurantException;
    List<MenuDTO> getAllMenu();
    Page<MenuDTO> getAllMenu(Optional<Integer> page, Optional<String> sortBy, Optional<Integer> size);
    MenuDTO findMenuById(int id) throws NotFoundException;
    MenuDTO updateMenu(int id, MenuDTO menuDTO) throws NotFoundException, RestaurantException;
    void deleteMenuById(int id) throws NotFoundException, RestaurantException;
}
