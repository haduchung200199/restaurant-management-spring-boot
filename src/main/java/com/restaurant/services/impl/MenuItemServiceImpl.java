package com.restaurant.services.impl;

import com.restaurant.dto.MenuItemDTO;
import com.restaurant.entity.Bill;
import com.restaurant.entity.MenuItem;
import com.restaurant.exception.message.ExceptionMessage;
import com.restaurant.exception.type.NotFoundException;
import com.restaurant.mapper.MenuItemMapper;
import com.restaurant.repository.BillRepository;
import com.restaurant.repository.MenuItemsRepository;
import com.restaurant.repository.MenuRepository;
import com.restaurant.services.MenuItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * class that implement MenuItemsServices
 */
@Service
public class MenuItemServiceImpl implements MenuItemService {
    @Autowired
    private MenuItemsRepository menuItemsRepository;
    @Autowired
    private MenuRepository menuRepository;
    @Autowired
    private BillRepository billRepository;
    @Autowired
    private MenuItemMapper menuItemMapper;

    private ExceptionMessage exceptionMessage = new ExceptionMessage();

    /**
     * Delete MenuItem by id
     */
    @Override
    public void deleteMenuItemById(int billId, int menuItemId) throws NotFoundException {
        if(billRepository.findById(billId).isEmpty()){
            throw new NotFoundException(exceptionMessage.CAN_NOT_FIND_BILL_EXCEPTION_MESSAGE);
        }
        if(menuItemsRepository.findById(menuItemId).isEmpty()){
            throw new NotFoundException(exceptionMessage.CAN_NOT_FIND_MENUITEM_EXCEPTION_MESSAGE);
        }

        MenuItem menuItem = menuItemsRepository.findById(menuItemId).get();
        Bill bill = billRepository.findById(billId).get();
        //calculate total price for Bill
        //total = total - price*quantity
        long total = bill.getTotalBill() -
                menuRepository.findById(menuItem.getMenu().getId()).get().getPrice() *
                menuItem.getQuantity();
        //set total for Bill
        bill.setTotalBill(total);

        menuItemsRepository.deleteById(menuItemId);
        billRepository.save(bill);
    }

    /**
     * Update MenuItem quantity by id
     */
    @Override
    public MenuItemDTO updateMenuItemById(int billId, int menuItemId, MenuItemDTO newMenuItemDTO) throws NotFoundException{
        if(billRepository.findById(billId).isEmpty()){
            //Can not find Bill
            throw new NotFoundException(exceptionMessage.CAN_NOT_FIND_BILL_EXCEPTION_MESSAGE);
        }
        if (menuItemsRepository.findById(menuItemId).isEmpty()){
            //Can not find menuItem
            throw new NotFoundException(exceptionMessage.CAN_NOT_FIND_MENUITEM_EXCEPTION_MESSAGE);
        }

        Bill oldBill = billRepository.findById(billId).get();
        long totalBill = oldBill.getTotalBill();
        MenuItem newMenuItem = menuItemMapper.dtoToEntity(newMenuItemDTO);
        MenuItem oldMenuItem = menuItemsRepository.findById(menuItemId).get();

        newMenuItem.setMenu(oldMenuItem.getMenu());
        //calculate totalBill. totalBill = totalBill -oldSubTotal + newSubTotal
        totalBill = totalBill - oldMenuItem.getMenu().getPrice() * oldMenuItem.getQuantity() +
               newMenuItem.getMenu().getPrice() * newMenuItem.getQuantity();
        //set total for Bill
        oldBill.setTotalBill(totalBill);
        newMenuItem.setBill(billRepository.save(oldBill));
        newMenuItem.setId(menuItemId);

        return menuItemMapper.entityToDTO(menuItemsRepository.save(newMenuItem));
    }
}
