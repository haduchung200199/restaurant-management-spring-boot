package com.restaurant.services.impl;

import com.restaurant.entity.Menu;
import com.restaurant.dto.MenuDTO;
import com.restaurant.entity.enumerate.MenuType;
import com.restaurant.exception.message.ExceptionMessage;
import com.restaurant.exception.type.NotFoundException;
import com.restaurant.exception.type.RestaurantException;
import com.restaurant.mapper.MenuMapper;
import com.restaurant.repository.MenuItemsRepository;
import com.restaurant.repository.MenuRepository;
import com.restaurant.services.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import static com.restaurant.mapper.GenericMapper.DEFAULT_PAGE_NUMBER;
import static com.restaurant.mapper.GenericMapper.DEFAULT_PAGE_SIZE;
import static com.restaurant.mapper.GenericMapper.ID;
/**
 * class that implement MenuService to make action for MenuController
 */
@Service
public class MenuServiceImpl implements MenuService {
    @Autowired
    private MenuRepository menuRepository;
    @Autowired
    private MenuItemsRepository menuItemsRepository;
    @Autowired
    private MenuMapper menuMapper;

    private ExceptionMessage exceptionMessage = new ExceptionMessage();
    /**
     * create new menu (save to database)
     */
    @Override
    public MenuDTO createNewMenu(MenuDTO menuDTO) throws RestaurantException {
        Menu menu = menuMapper.dtoToEntity(menuDTO);
        if(menuRepository.findMenuByName(menu.getName()).isPresent()){
            //Menu is already exist
            throw new RestaurantException(exceptionMessage.MENU_EXIST_EXCEPTION_MESSAGE);
        }
        //check if MenuType is not invalid
        boolean checkNotBreak = true;
        for (MenuType menuType : MenuType.values()){
            if(menuType.toString().equalsIgnoreCase(menu.getType())){
                checkNotBreak = false;
                break;
            }
        }
        if(checkNotBreak){
            //if no matching menu type
            throw new RestaurantException(exceptionMessage.WRONG_MENU_TYPE_EXCEPTION_MESSAGE);
        }

        return menuMapper.entityToDTO(menuRepository.save(menu));
    }

    /**
     * get all menu from database
     */
    @Override
    public List<MenuDTO> getAllMenu() {
        return menuMapper.entityToDTO(menuRepository.findAll());
    }

    /**
     * GetAll Menu with paging
     */
    @Override
    public Page<MenuDTO> getAllMenu(Optional<Integer> page, Optional<String> sortBy, Optional<Integer> size) {
        return menuMapper.entityToDTO(menuRepository.findAll(
                PageRequest.of(
                        page.orElse(DEFAULT_PAGE_NUMBER),
                        size.orElse(DEFAULT_PAGE_SIZE),
                        Sort.Direction.ASC,sortBy.orElse(ID))));
    }

    /**
     * search for menu by ID
     */
    @Override
    public MenuDTO findMenuById(int id) throws NotFoundException {
        if(menuRepository.findById(id).isEmpty()) {
            //Can not find Menu
            throw new NotFoundException(exceptionMessage.CAN_NOT_FIND_MENU_EXCEPTION_MESSAGE);
        }

        return menuMapper.entityToDTO(menuRepository.findById(id).get());
    }

    /**
     * delete menu byId
     */
    @Override
    public void deleteMenuById(int id) throws NotFoundException, RestaurantException{
        if(menuRepository.findById(id).isEmpty()) {
            //Can not find Menu
            throw new NotFoundException(exceptionMessage.CAN_NOT_FIND_MENU_EXCEPTION_MESSAGE);
        }
        if(menuItemsRepository.findMenuItemByMenuId(id).iterator().hasNext()){
            //Menu is being used
            throw new RestaurantException(exceptionMessage.MENU_IS_BEING_USED_EXCEPTION_MESSAGE);
        }

        menuRepository.deleteById(id);
    }

    /**
     * update menu adjust oldMenu using newMenu from API
     */
    @Override
    public MenuDTO updateMenu(int id, MenuDTO menuDTO) throws NotFoundException, RestaurantException{
        Menu oldMenu = menuRepository.findById(id).get();
        if(menuRepository.findById(id).isEmpty()) {
            throw new NotFoundException(exceptionMessage.CAN_NOT_FIND_MENU_EXCEPTION_MESSAGE);
        }

        Menu newMenu = menuMapper.dtoToEntity(menuDTO);
        if(!oldMenu.getName().equalsIgnoreCase(newMenu.getName())){
            if(menuRepository.findMenuByName(newMenu.getName()).isPresent()){
                throw new RestaurantException(exceptionMessage.MENU_EXIST_EXCEPTION_MESSAGE);
            }
        }
        //check if menuType is valid
        boolean checkNotBreak = true;
        for (MenuType menuType : MenuType.values()){
            if(menuType.toString().equalsIgnoreCase(newMenu.getType())){
                checkNotBreak = false;
                break;
            }
        }
        if(checkNotBreak){
            //if no matching menu type
            throw new RestaurantException(exceptionMessage.WRONG_MENU_TYPE_EXCEPTION_MESSAGE);
        }
        return menuMapper.entityToDTO(menuRepository.save(
                new Menu(id,newMenu.getName(),newMenu.getType(),newMenu.getPrice(),newMenu.getDescription())));
    }
}
