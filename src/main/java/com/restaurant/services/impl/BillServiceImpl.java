package com.restaurant.services.impl;

import com.restaurant.entity.Bill;
import com.restaurant.dto.BillDTO;
import com.restaurant.entity.Menu;
import com.restaurant.entity.MenuItem;
import com.restaurant.exception.message.ExceptionMessage;
import com.restaurant.exception.type.NotFoundException;
import com.restaurant.exception.type.RestaurantException;
import com.restaurant.mapper.BillMapper;
import com.restaurant.repository.BillRepository;
import com.restaurant.repository.MenuItemsRepository;
import com.restaurant.repository.MenuRepository;
import com.restaurant.services.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.restaurant.mapper.GenericMapper.DEFAULT_PAGE_NUMBER;
import static com.restaurant.mapper.GenericMapper.DEFAULT_PAGE_SIZE;
import static com.restaurant.mapper.GenericMapper.ID;

/**
 * class that implement BillServices to make action for BillController
 */
@Service
public class BillServiceImpl implements BillService {
    @Autowired
    private BillRepository billRepository;
    @Autowired
    private MenuItemsRepository menuItemsRepository;
    @Autowired
    private MenuRepository menuRepository;
    @Autowired
    private BillMapper billMapper;

    private ExceptionMessage exceptionMessage = new ExceptionMessage();
    //Format date-time for orderTime
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    /**
     * Create new bill
     */
    @Override
    public BillDTO createNewBill(){
        //for mat date-time
        SimpleDateFormat formatter= new SimpleDateFormat(DATE_FORMAT);
        Date date = new Date(System.currentTimeMillis());
        Bill bill = new Bill();
        bill.setOrderTime(formatter.format(date)) ;
        bill.setTotalBill(0L);
        return billMapper.entityToDTO(billRepository.save(bill));
    }

    /**
     *Find all bills
     */
    @Override
    public List<BillDTO> getAllBill() {
        List<Bill> billList = billRepository.findAll();

        return billMapper.entityToDTO(billList);
    }

    /**
     * Get all Bill with Paging
     */
    @Override
    public Page<BillDTO> getAllBill(Optional<Integer> page, Optional<String> sortBy, Optional<Integer> size) {
        return billMapper.entityToDTO(billRepository.findAll(
                PageRequest.of(
                        page.orElse(DEFAULT_PAGE_NUMBER),
                        size.orElse(DEFAULT_PAGE_SIZE),
                        Sort.Direction.ASC,sortBy.orElse(ID))));
    }

    /**
     * find Bill by id
     */
    @Override
    public BillDTO findBillById(int id) throws NotFoundException {
        Optional<Bill> billOptional = billRepository.findById(id);
        if(billOptional.isEmpty()){
            //can not find Bill
            throw new NotFoundException(exceptionMessage.CAN_NOT_FIND_BILL_EXCEPTION_MESSAGE);
        }

        Bill bill = billOptional.get();
        BillDTO billDTO = billMapper.entityToDTO(bill);
        return billDTO;
    }

    /**
     * Delete Bill by BillId
     */
    @Override
    public void deleteBillById(int id) throws NotFoundException, RestaurantException{
        if(billRepository.findById(id).isEmpty()){
            //Can not find bill
            throw new NotFoundException(exceptionMessage.CAN_NOT_FIND_BILL_EXCEPTION_MESSAGE);
        }
        if(menuItemsRepository.findMenuItemByBillId(id).iterator().hasNext()){
            //Can not delete if menuItemIterable has Bill
            throw new RestaurantException(exceptionMessage.BILL_IS_BEING_USED_EXCEPTION_MESSAGE);
        }

        billRepository.deleteById(id);
    }

    /**
     * Add menuItem to Bill
     */
    @Override
    public BillDTO addMenuitemToBill(int id, BillDTO billDTO) throws NotFoundException, RestaurantException{
        //check Bill exist
        if(billRepository.findById(id).isEmpty()){
            throw new NotFoundException(exceptionMessage.CAN_NOT_FIND_BILL_EXCEPTION_MESSAGE);
        }
        Bill oldBill = billRepository.findById(id).get();
        Bill newBill = billMapper.dtoToEntity(billDTO);
        List<MenuItem> newMenuItemList = new ArrayList<>();
        newMenuItemList.addAll(billDTO.getMenuItem());

        //list for deleting similar menu in newMenuItemList
        List<MenuItem> newMenuItemDeleteList = new ArrayList<>();
        long totalBill = oldBill.getTotalBill();
        for (MenuItem newMenuItem : newMenuItemList) {
            //check if the @PathVariable ID != BillID
            if (newMenuItem.getBill().getId()!=id){
                //the @PathVariable ID != BillID
                throw new RestaurantException(exceptionMessage.WRONG_BILL_ID_EXCEPTION_MESSAGE);
            }
            //check if menu exist
            if(menuRepository.findById(newMenuItem.getMenu().getId()).isEmpty()){
                // Menu does not exist
                throw new NotFoundException(exceptionMessage.CAN_NOT_FIND_MENU_EXCEPTION_MESSAGE);
            }
            //check for the same menu in menuItems in the same Bill
            Menu menu = menuRepository.findById(newMenuItem.getMenu().getId()).get();
            for (MenuItem oldMenuItem : oldBill.getMenuItem()) {
                if (newMenuItem.getMenu().getId() == oldMenuItem.getMenu().getId()) {
                    newMenuItemDeleteList.add(newMenuItem);
                    //update quantity
                    oldMenuItem.setQuantity(newMenuItem.getQuantity() + oldMenuItem.getQuantity());
                    break;
                }
            }
            //calculate totalBill.
            totalBill = totalBill + menu.getPrice() * newMenuItem.getQuantity();
        }
        //delete the same menu in menuItems in the same bill
        newMenuItemList.removeAll(newMenuItemDeleteList);
        menuItemsRepository.deleteAll(newMenuItemDeleteList);
        //set menu to menuItem
        for(MenuItem menuItem: newMenuItemList){
            menuItem.setMenu(menuRepository.findById(menuItem.getMenu().getId()).get());
        }
        menuItemsRepository.saveAll(newMenuItemList);

        //update Bil
        newBill.setOrderTime(oldBill.getOrderTime());
        newBill.setId(id);
        newBill.setTotalBill(totalBill);
        //update menuItem in bill
        Set<MenuItem> menuItemSet = oldBill.getMenuItem();
        for(MenuItem menuItem: newMenuItemList) {
            menuItemSet.add(menuItem);
        }
        newBill.setMenuItem(menuItemSet);
        
        return billMapper.entityToDTO(billRepository.save(newBill));
    }
}
