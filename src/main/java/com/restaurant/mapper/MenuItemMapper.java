package com.restaurant.mapper;

import com.restaurant.dto.MenuDTO;
import com.restaurant.dto.MenuItemDTO;
import com.restaurant.entity.Menu;
import com.restaurant.entity.MenuItem;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Mapper for MenuItem
 */
@Component
public class MenuItemMapper extends GenericMapper<MenuItem,MenuItemDTO>{
    private ModelMapper modelMapper = new ModelMapper();
    /**
     *Convert from MenuItemDTO to MenuItems
     */
    @Override
    public MenuItem dtoToEntity(MenuItemDTO menuItemDTO){
        return modelMapper.map(menuItemDTO,MenuItem.class);
    }

    /**
     * Convert from MenuItem to MenuItemDTO
     */
    @Override
    public MenuItemDTO entityToDTO(MenuItem menuItem){
        return modelMapper.map(menuItem,MenuItemDTO.class);
    }

    /**
     * Convert MenuItem List to MenuItemDTO List
     */
    @Override
    public List<MenuItemDTO> entityToDTO(List<MenuItem> menuItemList){
        Type listType = new TypeToken<List<MenuItemDTO>>(){}.getType();

        return modelMapper.map(menuItemList,listType);
    }

    /**
     * Convert MenuItemDTO List to MenuItem List
     */
    @Override
    public List<MenuItem> dtoToEntity(List<MenuItemDTO> menuItemDTOList){
        Type listType = new TypeToken<List<MenuItem>>(){}.getType();
        return modelMapper.map(menuItemDTOList,listType);
    }

    /**
     *Paging for MenuItemDTO
     */
    @Override
    public Page<MenuItemDTO> entityToDTO(Page<MenuItem> menuItemPage) {
        Type pageType = new TypeToken<Page<MenuItem>>(){}.getType();

        return modelMapper.map(menuItemPage,pageType);
    }
}
