package com.restaurant.mapper;

import com.restaurant.dto.BillDTO;
import com.restaurant.entity.Bill;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Mapper for Bill
 */
@Component
public class BillMapper extends GenericMapper<Bill,BillDTO>{
    private ModelMapper modelMapper = new ModelMapper();
    /**
     *Convert from BillDTO to Bill
     */
    @Override
    public Bill dtoToEntity(BillDTO billDTO){

        return modelMapper.map(billDTO,Bill.class);
    }

    /**
     * Convert from Bill to BillDTO
     */
    @Override
    public BillDTO entityToDTO(Bill bill){
        return modelMapper.map(bill,BillDTO.class);
    }

    /**
     * Convert Bill List to BillDTO List
     */
    @Override
    public List<BillDTO> entityToDTO(List<Bill> billList){
        System.out.println(billList.get(1).getMenuItem());
        Type listType = new TypeToken<List<BillDTO>>(){}.getType();

        return modelMapper.map(billList,listType);
    }

    /**
     * Convert BillDTO List to Bill List
     */
    @Override
    public List<Bill> dtoToEntity(List<BillDTO> billDTOList){
        Type listType = new TypeToken<List<Bill>>(){}.getType();

        return modelMapper.map(billDTOList,listType);
    }

    /**
     * Paging for bill DTO
     */
    @Override
    public Page<BillDTO> entityToDTO(Page<Bill> billPage) {
        Type pageType = new TypeToken<Page<Bill>>(){}.getType();

        return modelMapper.map(billPage,pageType);
    }
}
