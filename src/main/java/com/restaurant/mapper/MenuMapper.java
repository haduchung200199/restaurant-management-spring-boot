package com.restaurant.mapper;
import com.restaurant.dto.MenuDTO;
import com.restaurant.entity.Menu;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Mapper for Menu
 */
@Component
public class MenuMapper extends GenericMapper<Menu,MenuDTO> {
    private ModelMapper modelMapper = new ModelMapper();
    /**
     *Convert from MenuDTO to Menu
     */
    @Override
    public Menu dtoToEntity(MenuDTO menuDTO){
        return modelMapper.map(menuDTO,Menu.class);
    }

    /**
     * Convert from Menu to MenuDTO
     */
    @Override
    public MenuDTO entityToDTO(Menu menu){
        return modelMapper.map(menu,MenuDTO.class);
    }

    /**
     * Convert Menu List to MenuDTO List
     */
    @Override
    public List<MenuDTO> entityToDTO(List<Menu> menuList){
        Type listType = new TypeToken<List<MenuDTO>>(){}.getType();

        return modelMapper.map(menuList,listType);
    }

    /**
     * Convert MenuDTO List to Menu List
     */
    @Override
    public List<Menu> dtoToEntity(List<MenuDTO> menuDTOList){
        Type listType = new TypeToken<List<Menu>>(){}.getType();

        return modelMapper.map(menuDTOList,listType);
    }

    /**
     * Paging for MenuDTO
     */
    @Override
    public Page<MenuDTO> entityToDTO(Page<Menu> menuPage) {
        Type pageType = new TypeToken<Page<Menu>>(){}.getType();

        return modelMapper.map(menuPage,pageType);
    }
}
