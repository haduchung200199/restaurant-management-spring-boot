package com.restaurant.mapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import java.util.List;

/**
 * Generic interface for Mapper Entity
 */
@Component
public abstract class GenericMapper<E,D> {
    public static final Integer DEFAULT_PAGE_NUMBER = 0;
    public static final Integer DEFAULT_PAGE_SIZE = 3;
    public static final String ID = "id";

    abstract E dtoToEntity(D dto);
    abstract D entityToDTO(E entity);
    abstract List<E> dtoToEntity(List<D> dList);
    abstract List<D> entityToDTO(List<E> eList);
    abstract Page<D> entityToDTO(Page<E> ePage);
}
