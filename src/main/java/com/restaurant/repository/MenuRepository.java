package com.restaurant.repository;

import com.restaurant.entity.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Optional;
import static com.restaurant.entity.Menu.NAME_COLUMN;

/**
 * Repository for menu to communicate with database
 */
@Repository
public interface MenuRepository extends JpaRepository<Menu, Integer> {
    @Query("from Menu m where m.name = :name")
    Optional<Menu> findMenuByName(@Param(NAME_COLUMN) String name);
}
