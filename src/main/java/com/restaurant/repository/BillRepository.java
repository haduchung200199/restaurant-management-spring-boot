package com.restaurant.repository;

import com.restaurant.entity.Bill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for Bill
 */
@Repository
public interface BillRepository extends JpaRepository<Bill,Integer> {
}
