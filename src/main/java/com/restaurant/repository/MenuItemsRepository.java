package com.restaurant.repository;

import com.restaurant.entity.MenuItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Repository for MenuItems
 */
@Repository
public interface MenuItemsRepository extends JpaRepository<MenuItem,Integer> {
    @Query("from MenuItem m where m.bill.id = :id")
    List<MenuItem> findMenuItemByBillId(int id);
    @Query("from MenuItem m where m .menu.id = :menuId")
    List<MenuItem> findMenuItemByMenuId(int menuId);

}
